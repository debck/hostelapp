
const express = require('express');
const session = require('express-session')
const app = express();
const passport = require('./auth/mypassport')

const db = require('./db/models').db;
app.use(express.json());
app.use(express.urlencoded({extended: true}));


var mysql = require('mysql');

var con = mysql.createConnection({
  host: "b5il0qlein4sgwwwc6wv-postgresql.services.clever-cloud.com",
  db:"b5il0qlein4sgwwwc6wv",
  user: "uwsmjemn6d3f5anjy1m1",
  password: "NFGqsSchKycHTstvvHj0",
  port: 5432
})


app.use(session({
  secret: 'something that should not be shared',
  resave: false,
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

app.use('/api', require('./routes/index'));

app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

app.post('/login', passport.authenticate('local', {failureRedirect: '/'}), (req, res) => {
  res.send({
    success: true,
    url: '/student/'
  });
})

app.get('/student', (req, res, next) => {
  if (!req.user) {
    console.log("ABC")
    return res.redirect('/');
  }
  next();
})

var port = process.env.PORT || 8888;

app.use(express.static(__dirname + '/public'));

app.listen(port, () => {
  console.log("Server Listening on port 8888");
})
